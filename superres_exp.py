
'''
Super-resolution radar paper: 
- Experiments for Figures 3 and 7
Reinhard Heckel, October 2015
'''

#import numpy as np
import itertools
from numpy import *
import sys as syst
from os.path import dirname
syst.path.append('./SPGL1_python_port')
from spgl1 import *
from spgl_aux import *


#######
# helper functions

def sinc(t):
	if t == 0:
		return 1
	else:
		return sin(pi*t)/(pi*t)

def SNR(x,y): # SNR in dB
	assert x.shape == y.shape
	return 10*log(linalg.norm(x)**2 / linalg.norm(y)**2)/log(10)

def addnoise(y,snr): # snr is in dB
	noisepower = linalg.norm(y)**2 / exp(log(10)*snr/10) 
	n = random.randn(len(y))
	n *= sqrt(noisepower)/linalg.norm(n)
	#print 'added noise with SNR: ', SNR(y,n)
	return y + n

#######

class System: 
	
	def __init__(self,N):
		self.N = N
		self.L = 2*N+1
	
	def random_init_ongrid(self,S,gridc_tau,gridc_nu,K_tau,K_nu):
		'''
		Generate S grid points uniformly at random on the grid ((gridc_tau*t, gridc_nu*f): t = 0,...,K_tau-1, f=0,...,K_nu-1). 
		'''
		taus = [float(k)*gridc_tau for k in random.permutation(K_tau)[:S]]
		nus = [float(k)*gridc_nu for k in random.permutation(K_nu)[:S]]
		self.bs = ones(S, dtype='complex')
		#self.bs = random.randn(S) + 1j*random.randn(S)
		#self.bs = self.bs/abs(self.bs)
		self.tfshifts = zip(taus,nus)

	def random_init_diag_spread(self,S,gridc_tau,gridc_nu):
		'''
		Generate S points as (gridc_tau*t, gridc_nu*t), t = 0,...,S. 
		'''
		taus = [float(k)*gridc_tau for k in range(S) ]
		nus = [float(k)*gridc_nu for k in range(S) ]
		self.bs = ones(S, dtype='complex')
		#self.bs = random.randn(S) + 1j*random.randn(S)
		#self.bs = self.bs/abs(self.bs)
		self.tfshifts = zip(taus,nus)


	def random_init(self,S,taumax,numax):
		'''
		Generate S points uniformaly at random in [0,taumax] x [0,numax]. 
		'''
		taus = random.rand(S)*taumax
		nus = random.rand(S)*numax
		self.bs = 1*ones(S, dtype='complex')
		#self.bs = random.randn(S) + 1j*random.randn(S)
		self.bs = self.bs/abs(self.bs)
		self.tfshifts = zip(taus,nus)

	def response(self,x):
		y = zeros(self.L,dtype='complex') 
		for p in range(-self.N,self.N+1):
			for (tau,nu),b in zip(self.tfshifts,self.bs):
				for ell in range(-self.N,self.N+1):
					y[p] += exp(2j*pi*p*nu)*self.__TDN( float(p-ell)/self.L - tau)*x[ell]
		return y

	def idealized_response(self,x,itstart=None):
		if itstart == None:
			itstart = False
		y = zeros(self.L,dtype='complex')
		if(itstart):
			iter_range = range(self.L) 
		else:
			iter_range = range(-self.N,self.N+1)
		for p in iter_range:
			for (tau,nu),b in zip(self.tfshifts,self.bs):
				for ell in iter_range:
					y[p] += b*exp(2j*pi*p*nu)*self.__DN( float(p-ell)/self.L - tau)*x[ell]
		return y

	def idealized_response_periodic(self,x):
		M = int( sqrt(self.L))
		xt = self.__Ttau( x[:M] , 0 )
		y = zeros(self.L,dtype='complex') 
		for (tau,nu),b in zip(self.tfshifts,self.bs):
			for p in range(M):
				xt = self.__Ttau( x[:M] ,tau*M)
				Tx = tile( xt , M) # = [x,x,x,....,x] 
				FTx = self.__Fnu(Tx,nu)
				y += FTx*b
		return y

	def __Ttau(self,x,tau):
		X = fft.fft(x)
		#for k in range(-self.N,self.N+1):
		for k in range(len(x)):
			X[k] *= exp(-2j*pi*tau*k)
		xx = fft.ifft(X)
		return xx

	def __Fnu(self,x,nu):
		#for p in range(-self.N,self.N+1):
		for p in range(len(x)):
			x[p] *= exp(2j*pi*p*nu)
		return x

	def __DN(self,t):
		if (t % 1) ==0:
			return 1
		else:
			return sin(pi*self.L*t)/(self.L*sin(pi*t))
	def __TDN(self,t):
		return sinc(self.L*(t-1)) + sinc(self.L*t) + sinc(self.L*(t+1))



class Identifier:
	'''
	The super-resolution radar identifier. The time-frequency shifts are identified by solving a regularized least-squares program
	'''
	def __init__(self,N,gridc_tau,gridc_nu,K_tau,K_nu):
		self.N = N
		self.L = 2*N+1
		self.K_tau = K_tau # Number of grid points of the fine grid in tau-direction
		self.K_nu = K_nu
		self.gridc_tau = gridc_tau
		self.gridc_nu = gridc_nu
		# Chose random probing signal with entries i.i.d. uniformly on the unit disc, scaled to have unit norm 
		x = random.randn(self.L) + 1j*random.randn(self.L)
		self.x = 1/sqrt(self.L)*x/abs(x) 

		# Build matrix with partial time-frequency shifts of x 
		self.R = zeros((self.L,self.K_tau*self.K_nu),dtype = 'complex')
		for m,n in itertools.product(range(self.K_tau) , range(self.K_nu) ):
			tau = float(m)*gridc_tau
			nu = float(n)*gridc_nu
			self.R[:, m + n*self.K_tau] = self.__Fnu(self.__Ttau(self.x,tau) , nu) 


	def estimate(self,y,S,sigma = None,search='keepdist'): 
		'''
		Given an observation y, recover S time-frequency shifts, store the corresponding pairs in self.tfrec.
		search = 'naive': take as estimates the tf-shifts corresponding to the S largest attenuation factors
		search = 'keepdist': ensure additionally a minimum distance of 1/L 
		'''
		if sigma==None: # Solve exactly
			s,resid,grad,info = spg_bp(self.R,y)
			print 'solver: BP'
		else: 
			assert(sigma>=0)
			# print 'solver: basis pursuit denoising'
			opts = spgSetParms({
				'iterations' :   1000*self.L  # Set the maximum number of iterations to a large number (default is 10*self.L)
				})
			s,resid,grad,info = spg_bpdn(self.R, y, sigma, opts ) # Call the SPGL1 solver 
		
		self.largest = -sort(-abs(s))[:2*S]
		
		# find positions of spices
		if search=='naive':
			slargest = argsort(-abs(s))[:S]
			self.tfrec = [] # recovered time-frequency shifts 
			for ind in slargest:
				trec = float(ind % self.K_tau ) * self.gridc_tau
				frec = float(ind / self.K_tau ) * self.gridc_nu
				self.tfrec.append((trec,frec))
		if search=='keepdist':
			# `smart' search for the positions of spices: ensure that detected spices are sufficiently distinct
			largest = argsort(-abs(s))
			self.tfrec = [] # recovered time-frequency shifts 
			for ind in largest:
				trec = float(ind % self.K_tau ) * self.gridc_tau 
				frec = float(ind / self.K_tau ) * self.gridc_nu
				if(len(self.tfrec) == 0):
					self.tfrec.append((trec,frec))
				else:
					dists = [linalg.norm( array( (trec,frec) ) - array(tfo)) for tfo in self.tfrec]
					if(min(dists) > 1.0/self.L):
						self.tfrec.append((trec,frec))
				if(len(self.tfrec) >= S ):
					break


	def evaluate(self,gtruth,fposdist=1.5):
		'''
		Compute the resolution error, defined as the average of:
		L*sqrt( (hat tau - tau)^2 + (hat nu - nu)^2 ), where (tau,nu) and (hat tau, hat nu) are the original and recovered time-frequency shifts, respectively.
		A false positive is an original tf-shift to which no tf-shifts in the estimated tf-shifts closer than fposdist/L can be found
		'''
		avdist = 0.0
		falsepos = 0
		for tf in gtruth: # for each original tf-shift, find the closest estimated tf-shift
			dists = [linalg.norm(array(tf) - array(tfe)) for tfe in self.tfrec]
			if min(dists) >= fposdist/self.L:
				falsepos += 1
			else:
				avdist += min(dists)*self.L / len(gtruth)
		return (avdist,falsepos)
			

	def __Ttau(self,x,tau):
		X = fft.fft(x)
		for k in range(-self.N,self.N+1):
			X[k] *= exp(-2j*pi*tau*k)
		xx = fft.ifft(X)
		return xx

	def __Fnu(self,x,nu):
		for p in range(-self.N,self.N+1):
			x[p] *= exp(2j*pi*p*nu)
		return x

#################################################

class MUSICIdentifier:
	def __init__(self,ND,gridc_tau,gridc_nu,K_tau,K_nu, x = None):
		self.N = ND
		self.M = 2*ND+1
		self.L = self.M**2
		self.K_tau = K_tau
		self.K_nu = K_nu
		self.gridc_nu = gridc_tau
		self.gridc_tau = gridc_nu
		if x == None:
			# Generate random probing signal with entries uniform on the unit disc, scaled to have unit norm. 
			x = random.randn(self.M) + 1j*random.randn(self.M)
			x = 1/sqrt(self.M)*x/abs(x) 

		self.x = tile( 1/sqrt(self.L)*x/abs(x) , self.M) # = [x,x,x,....,x] 

		# Generate matrix with partial time-frequency shifts of x
		self.R = zeros((self.M,self.K_tau*self.K_nu),dtype = 'complex')
		for m,n in itertools.product( range(self.K_tau) , range(self.K_nu) ):
			tau = float(self.M)*gridc_tau*float(m)
			nu = float(n)*gridc_nu 
			self.R[:, m + n*self.K_tau] = self.__Fnu(self.__Ttau(x,tau) , nu) 
		
	def estimate(self,y,S): 
		'''
		Estimate S time-frequency shifts from y with the MUSIC algorithm. 
		'''
		Y = matrix(reshape( y ,(self.M,self.M) ).T)		
		# Build covariance matrix C
		C = Y*conj(Y.T)
		# Select noise subspace, find eigenvector corresponding to the M-S smallest e-values
		w,U = linalg.eigh(C)
		U2 = U[:, argsort(w)[:(self.M-S)] ]
		correlations = linalg.norm(np.dot(np.conj(U2.T), self.R),axis=0)
		indices = np.argsort(correlations)[:S]
		
		# Find positions of spices
		self.tfrec = [] # Estimated time-frequency shifts 
		for ind in indices:
			trec = float(ind % self.K_tau )*self.gridc_tau
			frec = float(ind / self.K_tau )*self.gridc_nu
			self.tfrec.append((trec,frec))

	def evaluate(self,gtruth,fposdist=1.0):
		avdist = 0.0
		falsepos = 0
		for tf in gtruth: # For each original tf-shift, find the closest estimated tf-shift
			dists = [linalg.norm(array(tf) - array(tfe)) for tfe in self.tfrec]
			if min(dists) >= fposdist/self.L:
				falsepos += 1
			else:
				avdist += min(dists)*self.L / len(gtruth)
		return (avdist,falsepos)


	def __Ttau(self,x,tau):
		X = fft.fft(x)
		for k in range(self.M):
			X[k] *= exp(-2j*pi*tau*k)
		xx = fft.ifft(X)
		return xx

	def __Fnu(self,x,nu):
		for p in range(self.M):
			x[p] *= exp(2j*pi*p*nu)
		return x


######################################################### Tests 


def test_run():
	# Problem parameters
	N = 50
	L = 2*N+1
	S = 3
	taumaxc = 2 # by default equal to numax
	taumax = float(taumaxc)/sqrt(L) # tau in [0, taumax], taumax <=1 
	SRF = 5
	gridc = 1.0/(SRF*L)
	K =  int(floor(taumax/gridc))
	sys = System(N) 
	
	print 'Generate', S, 'random points on the fine grid with SRF', SRF
	sys.random_init_ongrid(S,gridc,gridc,K,K)
	ide = Identifier(N,gridc,gridc,K,K)
	yideal = sys.idealized_response(ide.x)
	#ide.estimate(yideal,S) # via l1 minimization
	sigma = 0.000000000001
	ide.estimate(yideal,S,sigma) # via l1 minimization
	(av,fp) = ide.evaluate(sys.tfshifts)
	print 'Idealized response. Resolution error, false positives:', av, fp, '(=0 if perfect recovery on the fine grid)'
	y = sys.response(ide.x)
	sigma = 0.0001*linalg.norm(y)/sqrt(L)
	ide.estimate(y,S,sigma)
	# print 'norm(y-yorig)/norm(yorig) ', linalg.norm(y-yideal)/linalg.norm(yideal)
	print 'Taking standard for idealized response correspondst to SNR:', SNR(yideal, y-yideal)
	(av,fp) = ide.evaluate(sys.tfshifts)
	print 'Standard response. Resolution error, false positives:', av,fp, ' (=0 if perfect recovery on the fine grid)'

	print 'Generate ', S, ' random points off the grid '
	sys.random_init(S,taumax,taumax)
	print "Original tf-shifts:", sys.tfshifts
	sigma = 0.01*linalg.norm(y)/sqrt(L)
	y = sys.response(ide.x)
	ide.estimate(y,S,sigma)
	print "Estimated tf-shifts: ", ide.tfrec
	(av,fp) = ide.evaluate(sys.tfshifts)
	print 'Resolution error, false positives:',av,fp, '(= proportional to', 1.0/SRF, ')'

##################################

def MUSIC_test_run(): 
	# Problem parameters
	ND = 8 # -> L=289
	M = 2*ND+1
	N = M**2/2
	L = 2*N+1

	S = 2
	taumax = 1/float(M) # tau in [0, taumax], taumax <=1 
	numax = 1/float(M)
	SRF = 8
	K_tau = SRF*M
	K_nu = SRF*M
	gridc_tau = taumax/float(K_tau)
	gridc_nu = numax/float(K_nu)

	sys = System(N)
	sys.random_init_diag_spread(S,gridc_tau*SRF,gridc_nu*SRF)
	print 'Original time-frequency shifts of the system: ', sys.tfshifts
	mide1 = MUSICIdentifier(ND,gridc_tau,gridc_nu,K_tau,K_nu)
	my = sys.idealized_response(mide1.x,True) #my20 = addnoise(y,snr)
	my2 = sys.idealized_response_periodic(mide1.x)

	mide1.estimate(my2,S)
	print 'MUSIC. Estimated time-frequency shifts: ', mide1.tfrec 
	print 'MUSIC. Resolution error, false positives: ', mide1.evaluate(sys.tfshifts)

######################################################### Experiments





# Comparison of MUSIC and the convex programming approach for the super-resolution radar problem
def noise_music_comp(S,numit):
	print '----- Experiment with: ', S, numit 
	# Problem parameters - spread natural, corse grid
	ND = 8 # -> L=289
	M = 2*ND+1
	N = M**2/2
	L = 2*N+1
	taumax = 1/float(M) # tau in [0, taumax], taumax <=1 
	numax = 1/float(M)
	SRF = 6
	K_tau = SRF*M
	K_nu = SRF*M
	gridc_tau = taumax/float(K_tau)
	gridc_nu = numax/float(K_nu)
	snrs = [50,40,30,25,20,15,10,5,3]
	results = zeros((len(snrs), 2))
	falsepositives = np.zeros((len(snrs),2))
	for i,snr in enumerate(snrs):
		print 'SNR: ', snr
		for nit in range(numit):
			sys = System(N)
			sys.random_init_diag_spread(S,gridc_tau*SRF,gridc_nu*SRF)
			#sys.random_init(S,taumax,numax)
			### MUSIC
			xxx = random.randn(M) + 1j*random.randn(M)
			xxx = 1/sqrt(M)*xxx / abs(xxx)
			mide = MUSICIdentifier(ND,gridc_tau,gridc_nu,K_tau,K_nu,xxx)
			my = sys.idealized_response(mide.x,True) #my20 = addnoise(y,snr)
			my10 = addnoise(my,snr)
			mide.estimate(my10,S)
			(av,fp) = mide.evaluate(sys.tfshifts,float(L))
			results[i,0] += av/numit
			falsepositives[i,0] += float(fp)/numit
			print 'MUSIC: error and false-positives: ', av, fp
			### l1-minimization 
			ide = Identifier(N,gridc_tau,gridc_nu,K_tau,K_nu)
			y = sys.idealized_response(ide.x,False)
			y10 = addnoise(y,snr)
			sigma = 0.025*linalg.norm(y10)			
			ide.estimate(y10,S,sigma,'naive')
			(av,fp) = ide.evaluate(sys.tfshifts,float(L))
			print 'l1-minimization: error and false-positives: ', av, fp
			results[i,1] +=  av/numit
			falsepositives[i,1] += float(fp)/numit
	results = vstack((snrs, results.T,falsepositives.T )).T
	return results


def figure7_experiment(highresolution=True):
	if highresolution:
		numit = 100
		print '--- Generate Data for Figure 7 in high resolution ---'
	else:
		print '--- Generate Data for Figure 7 in low resolution ---'
		numit = 3
	resultsSRF_16 = noise_music_comp(16,numit)
	resultsSRF_4 = noise_music_comp(4,numit)
	resultsSRF_1 = noise_music_comp(1,numit)

	savetxt('./dat/resultsSRF_16_nfp.dat', resultsSRF_16, delimiter=' ')
	savetxt('./dat/resultsSRF_4_nfp.dat', resultsSRF_4, delimiter=' ')
	savetxt('./dat/resultsSRF_1_nfp.dat', resultsSRF_1, delimiter=' ')


def experiment_off_the_grid(highresolution=True):
	'''
	support recovery via BP denoising off the grid 
	'''
	# Problem parameters
	N = 100
	L = 2*N+1
	S = 10
	taumax = 2.0/sqrt(L) # tau in [0, taumax], taumax <=1 
	
	if highresolution: # generate the version in the paper
		print '--- Generate Data for Figure 3 in high resolution ---'
		numit = 20
		SRFrange = range(1,21)
	else: # generate a quick plot
		print '--- Generate Data for Figure 3 in low resolution ---'
		numit = 5
		SRFrange = [1,3,6,9]
	
	results = np.zeros((len(SRFrange),6))
	falsepositives = np.zeros((len(SRFrange),6))

	for q in range(numit):
		# Generate a random system
		sys = System(N) 
		sys.random_init(S,taumax,taumax)
		print '--------------- at iteration:', q 
		for j, SRF in enumerate(SRFrange):
			print '--- at SRF:', SRF
			results[j,0] = SRF
			gridc = 1.0/(SRF*L)
			K =  int(floor(taumax/gridc))
			ide = Identifier(N,gridc,gridc,K,K)
			#ide = Identifier(N,taumax,SRF)
			# noiseless, idealized 	
			yideal = sys.idealized_response(ide.x)
			sigma = 0.01*linalg.norm(yideal)/sqrt(SRF*L)
			ide.estimate(yideal,S,sigma)
			(av,fp) = ide.evaluate(sys.tfshifts)
			results[j,1] += av/numit
			falsepositives[j,1] += float(fp)/numit
			
			# noiseless
			y = sys.response(ide.x)
			sigma = 0.005*linalg.norm(y)
			ide.estimate(y,S,sigma)
			(av,fp) = ide.evaluate(sys.tfshifts)
			results[j,2] += av/numit
			falsepositives[j,2] += float(fp)/numit
			print 'SNR yideal, y : ', SNR(yideal, y-yideal)
			
			# 10dB noise
			snr = 10
			y10 = addnoise(y,snr)
			sigma = 0.05*linalg.norm(y10)
			print 'sigma 10dB: ', sigma
			ide.estimate(y10,S,sigma)
			(av,fp) = ide.evaluate(sys.tfshifts)
			results[j,3] += av/numit
			falsepositives[j,3] += float(fp)/numit
			
			# 20dB noise
			snr = 20
			y20 = addnoise(y,snr)
			sigma = 0.05*linalg.norm(y20)	
			ide.estimate(y20,S,sigma)
			(av,fp) = ide.evaluate(sys.tfshifts)
			results[j,4] += av/numit
			falsepositives[j,4] += float(fp)/numit
			
			# 30dB noise
			snr = 30
			y30 = addnoise(y,snr)
			sigma = 0.05*linalg.norm(y30)
			ide.estimate(y30,S,sigma)
			(av,fp) = ide.evaluate(sys.tfshifts)
			results[j,5] += av/numit
			falsepositives[j,5] += float(fp)/numit
	
	savetxt('./dat/offgrid.dat', results, delimiter=' ')
	print 'results:\n', results
	print 'falspositives:\n', falsepositives 
	return results,falsepositives





