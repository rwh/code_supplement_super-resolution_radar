function [ G ] = gabor2( a )
% Gabor matrix with window a

L = length(a);
N = (L-1)/2;


for p=-N:N
    for el=-N:N
        for r=-N:N
        
            G(p+N+1, (r+N)*L + el + N + 1) = a(mod(p-el+N, L)+1)*exp(1i*2*pi*r*p/L);
        
        end
    end
end




end

