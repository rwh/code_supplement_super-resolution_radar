function [F] = fourier(N)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

L = 2*N+1;

for m=-N:N
    for n=-N:N
    
    F(m+1+N,n+1+N) = exp(-1i*2*pi*m*n/L);
    
    end
end

F = 1/(2*N+1)*F;


end

