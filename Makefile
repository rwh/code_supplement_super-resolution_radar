

all: fig3 fig4 fig5 fig7

install:
	git clone https://github.com/drrelyea/SPGL1_python_port.git

fig3-orig:
	python -c 'from superres_exp import *; experiment_off_the_grid(highresolution=True)'
	cd ./tex; pdflatex figure3.tex

fig3:
	python -c 'from superres_exp import *; experiment_off_the_grid(highresolution=False)'
	cd ./tex; pdflatex figure3.tex

fig4: 
	matlab -nodesktop -nosplash -r gabor_sdp # generates: ../dat/dualpoly.dat
	cd ./tex; pdflatex figure4.tex

fig5: 
	matlab -nodesktop -nosplash -r gabor_sdp_noisy # generates: ../dat/dualpoly_noisy.dat, ..
	cd ./tex; pdflatex figure5.tex

fig7-orig:
	python -c 'from superres_exp import *; figure7_experiment(highresolution=True)' # generates: ./dat/resultsSRF_1_nfp.dat, ..
	cd ./tex; pdflatex figure7.tex

fig7:
	python -c 'from superres_exp import *; figure7_experiment(highresolution=False)' # generates: ./dat/resultsSRF_1_nfp.dat, ..
	cd ./tex; pdflatex figure7.tex

clean:
	find ./tex/ -type f ! -name '*.tex' -delete
	cd ./dat; rm *.dat

