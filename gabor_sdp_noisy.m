% Code to generate Figure 5 in ``Super-Resolution Radar'' by R. Heckel, V. Morgenshtern, M. Soltanolkotabi

clear all;
rng('default')

N = 5; 
L = 2*N+1;

rng(5); % seed random number generator

nspikes = 1;
nu = [0.5];
tau = [1-0.8];

% entries of b are iid uniform on the unit sphere
b = randn(1,nspikes) + i*randn(1,nspikes);
b = b./abs(b);


x = randn(1,L) + 1i*randn(1,L); % random probing signal
x = x./abs(x)/sqrt(L);



F1D = fourier(N);
F = kron(F1D,F1D);
G = gabor(x);

GFH = genGF(x);



y = zeros(L,1);
for p=-N:N
  
  for n=1:nspikes
    sum = 0;
    for el=-N:N
        for k=-N:N
            sum = sum + x(N+1+el)*exp(-1i*2*pi*k*tau(n))*exp(1i*2*pi*(p-el)*k/L);
        end
    end
    y(p+N+1) = y(p+N+1) + b(n)*exp(1i*2*pi*p*nu(n))*sum/L;
  
  end
  
end


%%%%

% Add 10 dB noise
snr = 10
%y = awgn(y,snr,'measured')
noisepower = norm(y)^2 / exp(log(10)*snr/10);
noise = randn(length(x),1);
noise = noise*sqrt(noisepower)/norm(noise);
y = y + noise

%%%%
    
delta = 0.8;

%% Solve SDP 

cvx_solver sdpt3
cvx_begin sdp 
    variable X(L^2+1,L^2+1) hermitian;
    variable c(L) complex;
    X >= 0;
    X(L^2+1,L^2+1) == 1;
    trace(X) == 2;
    GFH'*c == X(1:L^2,L^2+1);
    for k=(-L+1):L-1
    for q=(-L+1):L-1
        if( ~(k==0 && q ==0) )
            MQ = diag(ones(1,L-abs(q)),q);
            MK = diag(ones(1,L-abs(k)),k);
            trace(kron(MQ,MK)*X(1:L^2,1:L^2)) == 0;
        end
    end
    end
    
    maximize(real(c'*y) - delta*norm(c) )
cvx_end

% Construct dual polynomial from the solution of the SDP

st=[-N:N]';
st=[0:L-1]';

dualpoly = @(tauv,nuv) (GFH'*c)'*kron(exp(1i*2*pi*nuv*st), exp(1i*2*pi*tauv*st));

% Evaluate dual polynomial on a grid
t = linspace(0,1,100);

Z = [];

for m=1:length(t)
for n=1:length(t)
    Z(m,n) = dualpoly(t(m),t(n));
end
end

%surf(abs(Z))




%%% Generate data for the plot
% evaluate on a grid

t = linspace(0,1,100);
Z = [];
tau = linspace(0,1,61);
nu = linspace(0,1,61);
for m=1:length(tau)
for n=1:length(nu)
    Z(m,n) = dualpoly(nu(n),tau(m));
end
end

% surf(abs(Z))

surf2tikzf(abs(Z),tau,nu,'./dat/dualpoly_noisy.dat');

tau = linspace(0.78,0.82,61);
nu = linspace(0.48,0.52,61);
for m=1:length(tau)
for n=1:length(nu)
    Z(m,n) = dualpoly(nu(n),tau(m));
end
end

%surf(abs(Z))

surf2tikzf(abs(Z),tau,nu,'./dat/dualpoly_noisy_part.dat');

exit()



