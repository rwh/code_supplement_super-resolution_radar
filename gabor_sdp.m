
clear all;
rng('default')

N = 5; 
L = 2*N+1;

rng(5); % seed random number generator

nspikes = 2;
nu = [0.5,0.5];
tau = [0.2,0.8];

%nu = 0.5;
%tau = 0.3;

% entries of b are iid uniform on the unit sphere
b = randn(1,nspikes) + i*randn(1,nspikes);
b = b./abs(b);

a = randn(1,L) + 1i*randn(1,L); % random window
a = a./abs(a)/sqrt(L);

F1D = fourier(N);
F = kron(F1D,F1D);
G = gabor(a);

GFH = genGF(a);

y = zeros(L,1);
for p=-N:N
  
  for n=1:nspikes
    sum = 0;
    for el=-N:N
        for k=-N:N
            sum = sum + a(N+1+el)*exp(-1i*2*pi*k*tau(n))*exp(1i*2*pi*(p-el)*k/L);
        end
    end
    y(p+N+1) = y(p+N+1) + b(n)*exp(1i*2*pi*p*nu(n))*sum/L;
  
  end
  
end

%% Solve SDP 2D case

cvx_solver sdpt3
cvx_begin sdp 
    variable X(L^2+1,L^2+1) hermitian;
    variable c(L) complex;
    X >= 0;
    X(L^2+1,L^2+1) == 1;
    trace(X) == 2;
    
    GFH'*c == X(1:L^2,L^2+1);
    
    for k=(-L+1):L-1
    for q=(-L+1):L-1
        
        if( ~(k==0 && q ==0) )
            MQ = diag(ones(1,L-abs(q)),q);
            MK = diag(ones(1,L-abs(k)),k);
            trace(kron(MQ,MK)*X(1:L^2,1:L^2)) == 0;
        end
    end
    end
    
    maximize(real(c'*y))
cvx_end



%dual polynomial

st=[-N:N]';
st=[0:L-1]';

dualpoly = @(tauv,nuv) (GFH'*c)'*kron(exp(1i*2*pi*nuv*st), exp(1i*2*pi*tauv*st));


% evaluate on a grid
t = linspace(0,1,100);

Z = [];

for m=1:length(t)
for n=1:length(t)
    Z(m,n) = dualpoly(t(m),t(n));
end
end

% surf(abs(Z))

% write data 

t = linspace(0,1,500);
for m=1:length(t)
	y1(m) = dualpoly(0.5,t(m));
	y2(m) = dualpoly(t(m), 0.2 );
	y3(m) = dualpoly(t(m), 0.8 );
end
 
dlmwrite('./dat/dualpoly.dat',abs([t', y1',y2',y3']), ' ')

exit()

