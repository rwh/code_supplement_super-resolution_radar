function [] = surf2tikzf(Z,x,y,filename)
%

fid=fopen(filename, 'w');



for i=1:length(x)
    for j=1:length(y)
        str = sprintf('%1.4f\t%1.4f\t%1.4f',x(i),y(j), Z(i,j));
        fprintf(fid,'%s\n',str);
        % disp(str);
    end
    fprintf(fid,'\n');
end


 fclose(fid);

end
