
1. Introduction
===============

This folder contains the code to reproduce the numerical results reported in: 

**``Super-resolution radar''**, by R. Heckel, V.I. Morgenshtern, M. Soltanolkotabi

Specifically, the folder contains code to reproduce Figures 3, 4, 5, and 7, by running the corresponding instructions.
For example, `make fig4` will run the code to generate the data for Figure 4, stores the data in the ./dat folder, and generates the file ./tex/figure4.pdf containing the figure. 

2. Requirements
===============

Running the code requires installations of Python, Matlab, Make, and Pdflatex. 

The code for Figures 3 and 7 is written in Python. 
To solve the corresponding l_1-regularized least-squares problem, we use the SPGL1 solver, described in: 

E. van den Berg and M. P. Friedlander, "Probing the Pareto frontier for basis pursuit solutions", SIAM J. on Scientific Computing, 31(2):890-912, 2008.

The SPGL1 solver can be obtained from: `git clone https://github.com/drrelyea/SPGL1_python_port.git`

The code for Figures 4 and 5 is written in Matlab. 
To solve the corresponding SDP, we use CVX, which can be obained from: http://cvxr.com/cvx/

3. Quick start
==============

-	Obtain the SPGL1 solver by running the following command in a shell:
		`git clone https://github.com/drrelyea/SPGL1_python_port.git`
-   Install CVX in Matlab
-   Running: `make all` generates all figures, in low resolution.
-   Run:
	* `make fig3`		# to generate a low resolution version of Figure 3
	* `make fig3-orig` 	# to generate the original Figure 3
	* `make fig4` 		# to generate Figure 4 (with N=5 instead N=8)
	* `make fig5` 		# to generate Figure 5
	* `make fig7` 		# to generate a low resolution version of Figure 7
	* `make fig7-orig` 	# to generate the original Figure 7

4. Licence
==========

All files are provided under the terms of the Apache License, Version 2.0, see the included file "apache_licence_20" for details.
