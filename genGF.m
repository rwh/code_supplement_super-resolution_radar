function [ GF ] = genGF(a)
%


L = length(a);
N = (L-1)/2;

GF = zeros(L,L^2);

for p=-N:N
for k=-N:N
for q=-N:N
    
    sum = 0;
    for el=-N:N
    for r=-N:N
        
        sum =sum + a(mod(p-el+N, L)+1)*exp(1i*2*pi*r*p/L)*exp(1i*2*pi*(k*el+r*q)/L);
        
    end
    end
    
    GF(p+N+1, (k+N)*L + q + N + 1) = sum/L^2;
    
    
end
end
end

end



